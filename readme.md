#Mastering CSS Book

##The book
The code files that accompany Mastering CSS by Rich Finelli published thru Packt Publishing. For more information about the book, check out http://www.mastering-css.com/

##The video course
There is also a video course that I created - also named Mastering CSS  - and is also published thru Packt Publishing which has a slightly different set of code files, which can be found here: https://bitbucket.org/richfinelli/mastering-css-course

